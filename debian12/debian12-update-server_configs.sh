#!/usr/bin/bash

REPO_DIR="/server_configs"
USERNAME=$(realpath $REPO_DIR|cut -d'/' -f3)
sudo -u $USERNAME git -C "$REPO_DIR" pull

cp $REPO_DIR/server_docker/authorized_keys /home/$USERNAME/.ssh/authorized_keys
chown -R $USERNAME:$USERNAME /home/$USERNAME/.ssh/
chmod 700 /home/$USERNAME/.ssh/
chmod 600 /home/$USERNAME/.ssh/authorized_keys

cp $REPO_DIR/debian12/sshd_config /etc/ssh/sshd_config
chmod 644 /etc/ssh/sshd_config

systemctl enable docker

cp $REPO_DIR/debian12/debian12-update.service /etc/systemd/system/
cp $REPO_DIR/debian12/debian12-update.timer /etc/systemd/system/
cp $REPO_DIR/debian12/debian12-boot.service /etc/systemd/system/
systemctl enable debian12-update.timer
systemctl enable debian12-boot.service
