#!/usr/bin/bash

sudo -v
while true; do
  sleep 30
  sudo -v
  kill -0 "$$" 2>/dev/null || exit
done &

sudo apt update
sudo apt install docker.io git

echo "Change password for root"
sudo passwd root
if [ $(whoami) == "debian" ]; then
  echo "Change password for priviledged user: debian"
  passwd debian
else
  if [ $(whoami) != "root" ]; then
    echo "Need to run as either root or priviledged user, im neither root nor debian user"
    exit 1
  fi
fi

echo "Input new hostname:"
read newhostname
sudo hostnamectl set-hostname $newhostname

echo "Input new user name:"
read username
sudo adduser $username --home /home/$username

REPO_DIR=/home/$username/server_configs
sudo -u $username git clone https://gitlab.com/backpacknacho/server_configs.git $REPO_DIR
sudo -u $username git -C "$REPO_DIR" pull
sudo ln -sf $REPO_DIR /server_configs

sudo mkdir /home/$username/.ssh/
sudo cp $REPO_DIR/server_docker/authorized_keys /home/$username/.ssh/authorized_keys
sudo chown -R $username:$username /home/$username/.ssh/
sudo chmod 700 /home/$username/.ssh/
sudo chmod 600 /home/$username/.ssh/authorized_keys

sudo cp $REPO_DIR/debian12/sshd_config /etc/ssh/sshd_config
sudo chmod 644 /etc/ssh/sshd_config
sudo ssh-keygen -t rsa -b 4096 -f /etc/ssh/id_rsa_host -P ""

sudo systemctl enable sshd

sudo cp $REPO_DIR/debian12/debian12-update.service /etc/systemd/system/
sudo cp $REPO_DIR/debian12/debian12-update.timer /etc/systemd/system/
sudo cp $REPO_DIR/debian12/debian12-boot.service /etc/systemd/system/
sudo systemctl enable debian12-update.timer
sudo systemctl enable debian12-boot.service

echo "id_rsa_host.pub:"
sudo cat /etc/ssh/id_rsa_host.pub
echo "Press any key to reboot"
read anykeytoreboot
sudo reboot
