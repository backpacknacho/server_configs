#!/usr/bin/bash

if [ ! -e /server_configs ]; then
	echo "/server_configs not present"
fi

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
USER="neo"
DOCKER_LABEL="dockerserver"

cd $SCRIPT_DIR
cp /etc/ssh/id_rsa_host $SCRIPT_DIR/id_rsa
docker kill -s 9 $(docker ps -aq)
docker rm $(docker ps -aq)
docker image prune -af
docker build --rm --no-cache . -t $DOCKER_LABEL
rm $SCRIPT_DIR/id_rsa
docker run -d -v /server_configs:/server_configs -v /storage:/home/$USER/storage -p 60022:60022/tcp $DOCKER_LABEL
